package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class Sorteador {

	private List<Aluno> alunosEntrada = new ArrayList<>();
	private List<Equipe> ListaDeEquipes = new ArrayList<Equipe>();
	private int qtdeMaxEquipe;

	public Sorteador(List<Aluno> alunos) {
		this.alunosEntrada = alunos;
		this.qtdeMaxEquipe = 3;
	}

	public int sortearAluno() {
		int idAluno = (int) (Math.floor(Math.random() * alunosEntrada.size()));

		return idAluno;
	}

	public List<Equipe> montarEquipes() {
		int idEquipe = 0;
		int contadorAlunosEscolhidos = 0;
		int sizeLista = this.alunosEntrada.size();
		while (contadorAlunosEscolhidos < sizeLista) {
			Equipe equipeAux = new Equipe();

			for (int i = 0; i < qtdeMaxEquipe; i++) {
				equipeAux.alunos.add((alunosEntrada.remove(sortearAluno())));
				contadorAlunosEscolhidos++;
			}
			equipeAux.idEquipe = ++idEquipe;
			ListaDeEquipes.add(equipeAux);
			
		}
		return ListaDeEquipes;

	}
}