package com.itau.maratona;

import java.util.List;

public class Impressora {

	public Impressora() {

	}

	public static void imprimir(String valor) {
		System.out.print(valor);
	}

	public static void imprimir(int valor) {
		System.out.print(valor);
	}

	public static void imprimir(List<Equipe> equipes) {
		for (Equipe equipe : equipes) {
			System.out.println("Id: " + equipe.idEquipe);
			for (Aluno aluno : equipe.alunos) {
				System.out.println(aluno.nome);
			}
			System.out.println();
		}

	}

}
