package com.itau.maratona;

public class App {

	public static void main(String[] args) {

		Arquivo arquivo = new Arquivo("/home/a2/eclipse-workspace/103-maratona-programacao/src/alunos.csv");

		Sorteador sorteador = new Sorteador(arquivo.ler());

		Impressora.imprimir(sorteador.montarEquipes());
	}

}
